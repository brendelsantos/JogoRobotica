public enum Direcao {
	DIREITA(2), ESQUERDA(4), CIMA(0), BAIXO(6);
	private int value;

	private Direcao(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
