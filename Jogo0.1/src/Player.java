import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class Player extends Personagem {

	private int vida = 200;;
	
	public Player(int altura, int largura, int coordenadaX, int coordenadaY) {
		super(altura, largura, coordenadaX, coordenadaY);
		this.vida = 200;
		super.CarregaImagens("player");
	}

	public int getVida() {
		return vida;
	}

	public void setVida(int vida) {
		this.vida = vida;
	}

	public Tiro geraTiro(Direcao direcao) {
		Tiro novoTiro = new Tiro(getCoordenadaX(), getCoordenadaY(), direcao);
		return novoTiro;
	}

	
	
}
