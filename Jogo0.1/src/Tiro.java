import java.awt.Color;
import java.awt.Graphics;

public class Tiro {
	final int LARGURA = 30;
	final int ALTURA = 30;
	final int VELOCIDADE_TIRO = 5;
	final int VALOR_CORRECAO_X = 10;
	final int VALOR_CORRECAO_Y = 50;
	final int RAIO_TIRO = 8;

	private int coordenadaXTiro;
	private int coordenadaYTiro;
	private int inimigoAtingido;

	int altura;
	Direcao direcao;

	public Tiro(int coordenadaX, int coordenadaY, Direcao direcao) {
		this.coordenadaXTiro = coordenadaX;
		this.coordenadaYTiro = coordenadaY;
		this.direcao = direcao;
		this.inimigoAtingido = 0;
	}

	public void moveTiro() {
		if (direcao == Direcao.DIREITA)
			moveTiroDireita();
		if (direcao == Direcao.ESQUERDA)
			moveTiroEsquerda();
		if (direcao == Direcao.CIMA)
			moveTiroCima();
		if (direcao == Direcao.BAIXO)
			moveTiroBaixo();
	}

	public void moveTiroBaixo() {
		coordenadaYTiro += VELOCIDADE_TIRO;
	}

	public void moveTiroCima() {
		coordenadaYTiro -= VELOCIDADE_TIRO;
	}

	public void moveTiroDireita() {

		coordenadaXTiro += VELOCIDADE_TIRO;

	}

	public void moveTiroEsquerda() {
		coordenadaXTiro -= VELOCIDADE_TIRO;
	}

	public boolean testaPosicaoMatriz(CelulaMatriz[][] matrizMapa, int valorPersonagem) {
		boolean dentroMapa = (matrizMapa[coordenadaXTiro / VELOCIDADE_TIRO][coordenadaYTiro
				/ VELOCIDADE_TIRO].getValorCelula() == 0);
		boolean dentroAreaPersonagem = (matrizMapa[coordenadaXTiro
				/ VELOCIDADE_TIRO][coordenadaYTiro / VELOCIDADE_TIRO].getValorCelula() == valorPersonagem);
		if (dentroMapa || dentroAreaPersonagem) {
			return true;
		}
		int valorCoordenada = matrizMapa[coordenadaXTiro / VELOCIDADE_TIRO][coordenadaYTiro
				/ VELOCIDADE_TIRO].getValorCelula();
		if (valorCoordenada != 1)
			inimigoAtingido = valorCoordenada;
		return false;
	}

	public int getInimigoAtingido() {
		return inimigoAtingido;
	}

	public void pintaTiro(Graphics g) {
		g.setColor(Color.YELLOW);
		g.fillOval(coordenadaXTiro + VALOR_CORRECAO_X, coordenadaYTiro
				+ VALOR_CORRECAO_Y, RAIO_TIRO, RAIO_TIRO);
	}

}
