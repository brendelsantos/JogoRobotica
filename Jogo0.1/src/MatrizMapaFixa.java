
public class MatrizMapaFixa {
	private CelulaMatriz[][] matrizMapa;
	
	private int alturaMatriz;
	private int larguraMatriz;
	private int larguraPanel;
	private int alturaPanel;


	public MatrizMapaFixa(int laguraMatriz, int alturaMatriz, int larguraPanel, int alturaPanel ) {
		this.larguraMatriz = laguraMatriz;
		this.alturaMatriz = alturaMatriz;
		this.larguraPanel = larguraPanel;
		this.alturaPanel = alturaPanel;
		System.out.println(larguraPanel +" "+alturaPanel);
		matrizMapa = new CelulaMatriz[this.larguraMatriz][this.alturaMatriz];
		inicializaMatriz();
		preencheMatriz();
	}
	
	public void inicializaMatriz(){
		for (int i = 0; i < this.larguraMatriz; i++) {
			for (int j = 0; j < this.alturaMatriz; j++) {
				matrizMapa[i][j] = new CelulaMatriz(0, 0);
			}
		}
	}	
	
	public void preencheMatriz() {
		for (int i = 0; i < larguraMatriz; i++) {
			matrizMapa[i][0].setValorCelula(1);
			matrizMapa[i][alturaMatriz - 1].setValorCelula(1);
		}
		for (int i = 0; i < alturaMatriz; i++) {
			matrizMapa[0][i].setValorCelula(1);
			matrizMapa[larguraMatriz - 1][i].setValorCelula(1);
		}
		
		for (int i = 102; i < 153; i++) {
			for (int j = 0; j < 30; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i = 45; i < 93; i++) {
			for (int j = 25; j < 57; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i = 157; i < 205; i++) {
			for (int j = 74; j < 114; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i = 157; i < 170; i++) {
			for (int j = 74; j < 85; j++) {
				matrizMapa[i][j].setValorCelula(0);
			}
		}
		
		for (int i = 192; i < 205; i++) {
			for (int j = 74; j < 85; j++) {
				matrizMapa[i][j].setValorCelula(0);
			}
		}
		
		for (int i = 192; i < 205; i++) {
			for (int j = 106; j < 114; j++) {
				matrizMapa[i][j].setValorCelula(0);
			}
		}
		for (int i = 157; i < 168; i++) {
			for (int j = 105; j < 114; j++) {
				matrizMapa[i][j].setValorCelula(0);
			}
		}
		for (int i = 124; i < 150; i++) {
			for (int j = 81; j < 103; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		int inicioLargura = 137;
		int inicioAltura = 81;
		while (inicioLargura < 150 && inicioAltura < 96) {
			for (int i = inicioLargura; i < 150; i++) {
				matrizMapa[i][inicioAltura].setValorCelula(0);
			}
			inicioAltura++;
			inicioLargura++;
		}		
			
		inicioLargura = 137;
		inicioAltura = 81;
		while (inicioLargura >= 124 && inicioAltura < 96) {
			for (int i = inicioLargura; i >= 124; i--) {
				matrizMapa[i][inicioAltura].setValorCelula(0);
			}
			inicioAltura++;
			inicioLargura--;
		}	
		
		for (int i = 80; i < 89; i++) {
			for (int j = 99; j < 107; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i = 0; i < 7; i++) {
			for (int j = 94; j < alturaMatriz; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i = 0; i < 32; i++) {
			for (int j = 40; j < 85; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		inicioLargura = 21;
		inicioAltura = 39;
		while (inicioLargura < 32 && inicioAltura < 55) {
			for (int i = inicioLargura; i < 32; i++) {
				matrizMapa[i][inicioAltura].setValorCelula(0);
			}
			inicioAltura++;
			inicioLargura++;
		}	
		for (int i = 0; i < 22; i++) {
			for (int j = 85; j < 94; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = 171; i < 195; i++) {
			for (int j = 7; j < 26; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i = 198; i < 229; i++) {
			for (int j = 108; j < 139; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		inicioLargura = 228;
		inicioAltura = 108;
		while (inicioLargura >= 198 && inicioAltura <= 138) {
			for (int i = inicioLargura; i >= 198
					; i--) {
				matrizMapa[i][inicioAltura].setValorCelula(0);
			}
			inicioAltura++;
			inicioLargura--;
		}
		for (int i = 205; i < 229; i++) {
			for (int j = 88; j < 99; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = 0; i < 19; i++) {
			for (int j = 0; j < 39; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = 212; i < 229; i++) {
			for (int j = 1; j < 8; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = 29; i < 42; i++) {
			for (int j = 101; j < 108; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = 29; i < 53; i++) {
			for (int j = 112; j < 119; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = 29; i < 65; i++) {
			for (int j = 123; j < 130; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = 79; i < 91; i++) {
			for (int j = 132; j < 138; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = 127; i < 141; i++) {
			for (int j = 130; j < 137; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = 212; i < 225; i++) {
			for (int j = 10; j < 73; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = 198; i < 213; i++) {
			for (int j = 40; j < 60; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = 188; i < 199; i++) {
			for (int j = 40; j < 51; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = 129; i < 156; i++) {
			for (int j = 55; j < 78; j++) {
				matrizMapa[i][j].setCustoCelula(5);
			}
		}
		for (int i = 101; i < 113; i++) {
			for (int j = 66; j < 88; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = 83; i < 93; i++) {
			for (int j = 67; j < 79; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = 68; i < 80; i++) {
			for (int j = 69; j < 79; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = 42; i < 53; i++) {
			for (int j = 79; j < 90; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = 30; i < 65; i++) {
			for (int j = 87; j < 100; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = 42; i < 77; i++) {
			for (int j = 100; j < 110; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = 53; i < 86; i++) {
			for (int j = 111; j < 118; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = 117; i < 200; i++) {
			for (int j = 128; j < 138; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = 205; i < 225; i++) {
			for (int j = 100; j < 120; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
	}
	public int getLarguraPanel() {
		return larguraPanel;
	}

	public void setLarguraPanel(int larguraPanel) {
		this.larguraPanel = larguraPanel;
	}

	public int getAlturaPanel() {
		return alturaPanel;
	}

	public void setAlturaPanel(int alturaPanel) {
		this.alturaPanel = alturaPanel;
	}
	
	public CelulaMatriz[][] getMatrizMapa() {
		return matrizMapa;
	}
	
	public int porcentagemLargura(double valorPorcentagem){
		return (int)((valorPorcentagem / 100) * larguraPanel) ;
	}
	
	public int porcentagemAltura(double valorPorcentagem){
		return (int) ((valorPorcentagem / 100) * alturaPanel);
	}
	
	
	public CelulaMatriz[][] atualizaMatriz(){
		for (int i = 0; i < larguraMatriz; i++) {
			for (int j = 0; j < alturaMatriz; j++) {
				if(matrizMapa[i][j].getValorCelula() != 1 && matrizMapa[i][j].getValorCelula() != 0)
					matrizMapa[i][j].setValorCelula(0);;
			}
		}
		return matrizMapa;
	}

	public void imprimeMatriz(){
		for (int i = 0; i < larguraMatriz; i++) {
			for (int j = 0; j < alturaMatriz; j++) {
				System.out.print(matrizMapa[i][j].getCustoCelula());
			}
			System.out.println();
		}
		System.out.println();
	}
}
