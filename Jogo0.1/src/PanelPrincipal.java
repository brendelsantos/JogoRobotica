import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

public class PanelPrincipal extends JPanel implements KeyListener {

	final int LARGURA = 1200;
	final int ALTURA = 800;

	final int MOVE_COORDENADA = 5;
	final int FATOR_CORRECAO_LARGURA = 10;
	final int FATOR_CORRECAO_ALTURA = 20;
	final int PASSOS_COORDENADA = 5;
	final int LARGURA_MATRIZ = (LARGURA / PASSOS_COORDENADA)
			- FATOR_CORRECAO_LARGURA;
	final int ALTURA_MATRIZ = (ALTURA / PASSOS_COORDENADA)
			- FATOR_CORRECAO_ALTURA;
	int indiceInimigo = 2;
	int maximoInimigos = 2 + indiceInimigo;
	int delayTiro = 15;
	int delayPlayer = 30;
	int maximoTiro = 10;
	private int indiceTiro = 0;
	private Font fonteHelvetica;
	private Direcao direcaoPlayer;
	private Image background;
	private int quantidadeVidaPlayer;

	private int pontuacao;
	private Player player;
	private JProgressBar vidaPlayer;	
	private Tiro[] tiro;
	private MatrizMapa matriz;
	private MovimentoTeclado movimentoTeclado;
	private Grafo grafo;
	ArrayList<Thread> threads;
	LinkedMap<Integer, InimigoRobo> inimigos = new LinkedMap<>();
	protected boolean panelAtivo;

	public PanelPrincipal() {
		quantidadeVidaPlayer = 1000;
		panelAtivo = true;
		matriz = new MatrizMapa(LARGURA_MATRIZ, ALTURA_MATRIZ, LARGURA, ALTURA);

		setTiro(new Tiro[maximoTiro]);
		setPlayer(new Player(80, 40, 100*5, 70*5));
		getPlayer().setValorPersonagem(-2);
		movimentoTeclado = new MovimentoTeclado(new HashSet<Integer>(), this);
		atualizaPanel();
		movePlayer();
		moverTirosTimer();
		threads = new ArrayList<Thread>();
		setSize(LARGURA, ALTURA);
		abreBackGround();
		setVisible(true);
		this.setFocusable(true);
		this.requestFocusInWindow();
		grafo = new Grafo(matriz.getMatrizMapa(), MOVE_COORDENADA);
		grafo.adicionaVertice();
		grafo.adicionaAresta();
		pontuacao();
		geraInimigos();
		//startInimigos();
		inimigosThread();
		matriz.imprimeMatriz();
		
		
		
		vidaPlayer = new JProgressBar();
		vidaPlayer.setMaximum(1000);
		vidaPlayer.setValue(quantidadeVidaPlayer);
	
		this.add(vidaPlayer);
		
		fonteHelvetica = new Font("Helvetica",1, 14);
	}

	private void geraInimigos() {
	
		for (int i = indiceInimigo; i < maximoInimigos; i++) {
			int random1 = (int) (Math.random() * ((1000) + 1));
			int random2 = (int) (Math.random() * ((600) + 1));
			while (matriz.getMatrizMapa()[random1 / MOVE_COORDENADA][random2
					/ MOVE_COORDENADA].getValorCelula() != 0) {
				random1 = (int) (Math.random() * ((1000) + 1));
				random2 = (int) (Math.random() * ((600) + 1));
			}
			if (inimigos.get(i) == (null)) {
				inimigos.put(i, new InimigoRobo(80, 60, random1, random2, player, grafo));
				getInimigoRobo(i).setValorPersonagem(i);
				getInimigoRobo(i).setValorAdaptacao(MOVE_COORDENADA);
				threads.add(new Thread(inimigos.get(i)));
				startThreads();
			}	
		}
	}

	public void startThreads(){
		for (Thread thread : threads) {
			
			if(thread.getState() == Thread.State.NEW){
				thread.start();
			}
		}
	}
	
	public int getIndiceTiro() {
		return indiceTiro;
	}

	public void setIndiceTiro(int indiceTiro) {
		this.indiceTiro = indiceTiro;
	}

	public Direcao getDirecaoPlayer() {
		return direcaoPlayer;
	}

	public void setDirecaoPlayer(Direcao direcaoPlayer) {
		this.direcaoPlayer = direcaoPlayer;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public InimigoRobo getInimigoRobo(int indice) {
		return inimigos.get(indice);
	}

	public void removeInimigoRobo(int indice) {
		this.inimigos.get(indice).setValorPersonagem(0);
		this.inimigos.get(indice).setVida(0);
		this.inimigos.remove(indice);
	}

	public Tiro[] getTiro() {
		return tiro;
	}

	public void setTiro(Tiro[] tiro) {
		this.tiro = tiro;
	}

	 public void abreBackGround() {
		try {
			URL pathBackground = getClass().getResource("/images/Cenario1.png");
			background = ImageIO.read(pathBackground).getScaledInstance(
					LARGURA, ALTURA, background.SCALE_DEFAULT);

		} catch (IOException e) {
			e.printStackTrace();
		}
		addKeyListener(this);
	}

	public PanelPrincipal retornaPanel() {
		return this;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setFont(fonteHelvetica);  
		g.setColor(Color.white);
		g.drawImage(background, 0, 0, null);
		/*int x = LARGURA/5, y = 20, gap = 10,w = 150,h = 60; 
		for(int c=0;c<7;c++){
			
			g.fill3DRect(x+c+2*(w+gap),y+c,w-2*c,h-2*c, true);
		 
		}*/
		g.drawString("Pontua��o: "+String.valueOf(pontuacao), LARGURA/2-20 , 40);
		paintInimigos(g);
		getPlayer().paint(g);
		paintTiros(g);
		g.drawString("HP: ", LARGURA/2 -20, 60);
		vidaPlayer.setBounds( LARGURA/2 +5 , 45  , 150, 15);
		//g.setColor(Color.WHITE);
	}

	private void paintTiros(Graphics g) {
		for (int i = 0; i < getTiro().length; i++) {
			if (getTiro()[i] != null) {
				getTiro()[i].pintaTiro(g);
			}
		}
	}

	private void paintInimigos(Graphics g) {
		for (int i = indiceInimigo; i < maximoInimigos; i++) {
			if (getInimigoRobo(i) != null)
				getInimigoRobo(i).paint(g, retornaPanel());
		}
	}

	public void keyPressed(KeyEvent ke) {
		movimentoTeclado.getTeclasPressionadas().add(ke.getKeyCode());
	}

	@Override
	public void keyReleased(KeyEvent ke) {
		try {
			movimentoTeclado.geraTiro();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		movimentoTeclado.getTeclasPressionadas().remove(ke.getKeyCode());
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void movePlayer() {
		
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (panelAtivo) {
						mudaCoordenadaPlayer();
						Thread.sleep(delayPlayer);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		thread.start();
	}

	private void mudaCoordenadaPlayer() {
		int coordenadaY = getPlayer().getCoordenadaY();
		int coordenadaX = getPlayer().getCoordenadaX();
		coordenadaY = movimentoTeclado.moverCima(coordenadaY);
		coordenadaY = movimentoTeclado.moverBaixo(coordenadaY);
		coordenadaX = movimentoTeclado.moverEsquerda(coordenadaX);
		coordenadaX = movimentoTeclado.moverDireita(coordenadaX);
		alteraPosicaoPlayer(coordenadaY, coordenadaX);
	}

	private void alteraPosicaoPlayer(int coordenadaY, int coordenadaX) {
		getPlayer().alteraValorMatriz(matriz.getMatrizMapa());
		getPlayer().alteraPosicao(matriz.getMatrizMapa(), coordenadaX,
				coordenadaY);
	}

	public void moverTirosTimer() {
		TimerTask task = new TimerTask() {
			public void run() {
				moveTiros();
			}
		};
		Timer timer = new Timer();
		timer.schedule(task, 0, delayTiro);
	}

	public void atualizaPanel() {
		TimerTask task = new TimerTask() {
			public void run() {
				repaint();
				matriz.atualizaMatriz();
				if(vidaPlayer != null){
					vidaPlayer.setValue(quantidadeVidaPlayer);
					vidaPlayer.repaint();
				}
				if(quantidadeVidaPlayer <= 0){
					//TODO
					Window w = SwingUtilities.getWindowAncestor(retornaPanel());
					w.setVisible(false);
					panelAtivo = false;
					for (int i = indiceInimigo; i < maximoInimigos; i++) {
						getInimigoRobo(i).setThreadAtiva(false);
					}
					System.out.println(quantidadeVidaPlayer);
				}
			}
		};
		Timer timer = new Timer();
		timer.schedule(task, 0, 45);
	}

	
	private void moveTiros() {
		for (int i = 0; i < getTiro().length; i++) {
			if (getTiro()[i] != null) {
				boolean naoAtingiuInimigo = getTiro()[i].testaPosicaoMatriz(
						matriz.getMatrizMapa(), player.getValorPersonagem());
				if (naoAtingiuInimigo) {
					getTiro()[i].moveTiro();
				} else {
					int inimigoAtingido = getTiro()[i].getInimigoAtingido();
					if (inimigoAtingido != 0)
						getInimigoRobo(inimigoAtingido).consomeVida();
					getTiro()[i] = null;
				}
			}
		}
	}		
	
	public void inimigosThread() {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (panelAtivo) {
						threadPrincipalInimigos();
						Thread.sleep(delayPlayer);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		thread.start();
	}
	
	public void pontuacao(){
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(panelAtivo){
					
					pontuacao++;
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		thread.start();
	}

	private void threadPrincipalInimigos() {
		
		for (int i = indiceInimigo; i < maximoInimigos; i++) {
			if (getInimigoRobo(i) != null) {
				getInimigoRobo(i).anguloRelacaoPlayer(player.getCoordenadaX(),
						player.getCoordenadaY());
				getInimigoRobo(i).alteraValorMatriz(matriz.getMatrizMapa());
				getInimigoRobo(i).alteraPosicao(matriz.getMatrizMapa(),
						getInimigoRobo(i).getCoordenadaX(),
						getInimigoRobo(i).getCoordenadaY());
				
				if(getInimigoRobo(i).distanciaAtePlayer() < 45){
					quantidadeVidaPlayer-= 3;					
				}
				
				if (getInimigoRobo(i).getValorPersonagem() == 0) {
					try {
						Thread.sleep(40);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					maximoInimigos++;
					pontuacao += 10;
					removeInimigoRobo(i);
					geraInimigos();
					matriz.atualizaMatriz();

				}
				if(inimigos.isEmpty()){
					//geraInimigos();
				}
			}
		}
	}

	public static void main(String[] args) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		PanelPrincipal moveImagem = new PanelPrincipal();
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setSize(moveImagem.LARGURA, moveImagem.ALTURA+20);
		frame.add(moveImagem);
		frame.setVisible(true);
		moveImagem.requestFocusInWindow();
		SoundTest sd = new SoundTest();
		sd.load("somFundo.wav");
		sd.play(14);
	}
} 