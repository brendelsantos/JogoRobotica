import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.Semaphore;


public class CustoUniforme {
	Vertice inicial;
	Semaphore mutex = new Semaphore(1);
	
	public void custoUniforme(Vertice inicial, Vertice finalVertice, int pai){
		try {
			(mutex).acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.inicial = inicial;
		Comparator<Vertice> custoComparator = new CustoComparator();
		PriorityQueue<Vertice> filaVertice = new PriorityQueue<Vertice>(10, custoComparator);
		filaVertice.add(inicial);
		Vertice atual = null;
		while(!filaVertice.isEmpty() && atual != finalVertice){
			atual = filaVertice.poll();
			atual.setExpandido(true);
			for(Aresta aresta : atual.getAresta()){
				Vertice expandido = aresta.getDestino();
				
				if(!expandido.isExpandido()){
					
					int custo = aresta.getCusto();
					int custoTotal = atual.getCustoMinimo() + custo+ (int)distanciaEntreDoisPontos(expandido, atual);
					if((custoTotal < expandido.getCustoMinimo()) || (expandido.getCustoMinimo() == 0)){
						expandido.setCustoMinimo(custoTotal);
						expandido.setPai(atual, pai);
						filaVertice.add(expandido);
					}
				}
			}
		}
		(mutex).release();
	}
	
	double distanciaEntreDoisPontos(Vertice r1, Vertice r2){

	    return Math.sqrt((Math.pow((r2.getCoordenadaX()-r1.getCoordenadaX()),2))+
	                (Math.pow((r2.getCoordenadaY()-r1.getCoordenadaY()),2)));
	}
	
	public ArrayList<Vertice> geraCaminho(Vertice verticeFinal, int pai){
		try {
			(mutex).acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<Vertice> caminho = new ArrayList<Vertice>();

		while(verticeFinal != null){
				caminho.add(verticeFinal);
				verticeFinal = verticeFinal.getPai(pai);
			
		}
		Collections.reverse(caminho);
		if(caminho.size() <= 1){
			caminho.clear();
			caminho.add(inicial);
			
		}
		(mutex).release();
		return caminho;
	}
}
