import java.util.ArrayList;


public class Vertice {

	private String label;
	private int coordenadaX;
	private int coordenadaY;
	private Vertice[] pai;
	private Vertice[] filhos;
	private int custoMinimo;
	private boolean expandido;
	private ArrayList<Aresta> aresta;
	
	public Vertice(int coordenadaX, int coordenadaY) {
		this.label = coordenadaX+""+coordenadaY;
		this.coordenadaX = coordenadaX;
		this.coordenadaY = coordenadaY;
		pai = new Vertice[30];
		filhos = new Vertice[12];
		setPaisNull();
		this.custoMinimo = 0;
		this.expandido = false;
		this.aresta = new ArrayList<Aresta>();
	}
	
	public void setPaisNull(){
		for (int i = 0; i < pai.length; i++) {
			pai[i] = null;
		}
	}

	public int getCoordenadaX() {
		return coordenadaX;
	}

	public void setCoordenadaX(int coordenadaX) {
		this.coordenadaX = coordenadaX;
	}

	public int getCoordenadaY() {
		return coordenadaY;
	}

	public void setCoordenadaY(int coordenadaY) {
		this.coordenadaY = coordenadaY;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Vertice getPai(int indice) {
		return pai[indice];
	}
	public Vertice[] getPai() {
		return pai;
	}

	public void setPai(Vertice pai, int indice) {
		this.pai[indice] = pai;
	}
	
	public Vertice getFilho(int indice) {
		return filhos[indice];
	}
	public Vertice[] getFilhos() {
		return filhos;
	}

	public void setFilho(Vertice filho, int indice) {
		this.filhos[indice] = filho;
	}

	public int getCustoMinimo() {
		return custoMinimo;
	}

	public void setCustoMinimo(int custoMinimo) {
		this.custoMinimo = custoMinimo;
	}

	public boolean isExpandido() {
		return expandido;
	}

	public void setExpandido(boolean expandido) {
		this.expandido = expandido;
	}
	
	public Aresta getAresta(int indice){
		return this.aresta.get(indice);
	}
	
	public ArrayList<Aresta> getAresta(){
		return this.aresta;
	}
	
	public void addAresta(Aresta aresta){
		this.aresta.add(aresta);
	}
	
	public void exibeArestas(){
		System.out.println("Saida: "+coordenadaX + " " + coordenadaY);
		for (Aresta aresta : this.aresta) {
			System.out.print(" ("+aresta.getDestino().coordenadaX + " "+aresta.getDestino().coordenadaY+")");
		}
		System.out.println();
	}

}
