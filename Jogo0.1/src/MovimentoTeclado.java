import java.awt.event.KeyEvent;
import java.awt.peer.PanelPeer;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;


public class MovimentoTeclado {

	PanelPrincipal panelPrincipal;
	Direcao direcao;
	Player player;
	InimigoRobo inimigoRobo;
	
	private Set<Integer> teclasPressionadas = new HashSet<Integer>();
	
	public MovimentoTeclado(Set<Integer> teclasPressionadas, PanelPrincipal panelPrincipal) {
		this.teclasPressionadas = teclasPressionadas;
		this.panelPrincipal = panelPrincipal; 
		this.direcao = panelPrincipal.getDirecaoPlayer();
		this.player = panelPrincipal.getPlayer();
	}
	
	public Set<Integer> getTeclasPressionadas() {
		return teclasPressionadas;
	}
	
	int moverDireita(int coordenadaX) {
		if (teclasPressionadas.contains(KeyEvent.VK_D)
				|| teclasPressionadas.contains(KeyEvent.VK_RIGHT)) {
			coordenadaX += panelPrincipal.MOVE_COORDENADA;
			panelPrincipal.setDirecaoPlayer(Direcao.DIREITA);
			alteraImagem();
		}
		return coordenadaX;
	}
	
	int moverEsquerda(int coordenadaX) {
		if (teclasPressionadas.contains(KeyEvent.VK_A)
				|| teclasPressionadas.contains(KeyEvent.VK_LEFT)) {
			coordenadaX -= panelPrincipal.MOVE_COORDENADA;
			panelPrincipal.setDirecaoPlayer(Direcao.ESQUERDA);
			alteraImagem();
		}
		return coordenadaX;
	}
	
	int moverBaixo(int coordenadaY) {
		if (teclasPressionadas.contains(KeyEvent.VK_S)
				|| teclasPressionadas.contains(KeyEvent.VK_DOWN)) {
			coordenadaY += panelPrincipal.MOVE_COORDENADA;
			panelPrincipal.setDirecaoPlayer(Direcao.BAIXO);
			alteraImagem();
		}
		return coordenadaY;
	}

	int moverCima(int coordenadaY) {
		if (teclasPressionadas.contains(KeyEvent.VK_W)
				|| teclasPressionadas.contains(KeyEvent.VK_UP)) {
			coordenadaY -= panelPrincipal.MOVE_COORDENADA;
			panelPrincipal.setDirecaoPlayer(Direcao.CIMA);
			alteraImagem();
		}
		return coordenadaY;
	}

	private void alteraImagem() {
		direcao = panelPrincipal.getDirecaoPlayer();
		player.alteraImagem(direcao);
	}
	
	void geraTiro() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		if (teclasPressionadas.contains(KeyEvent.VK_SPACE) || teclasPressionadas.contains(KeyEvent.VK_ENTER) ) {
			if (panelPrincipal.getTiro()[panelPrincipal.getIndiceTiro()] == null){
				somTiro();
				panelPrincipal.getTiro()[panelPrincipal.getIndiceTiro()] = panelPrincipal.getPlayer().geraTiro(panelPrincipal.getDirecaoPlayer());
				panelPrincipal.setIndiceTiro(panelPrincipal.getIndiceTiro() + 1);
			}
			if (panelPrincipal.getIndiceTiro() == panelPrincipal.maximoTiro)
				panelPrincipal.setIndiceTiro(0);
		}
	}

	private void somTiro() {
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				SoundTest sd = new SoundTest();
				try {
					sd.load("bullet_.wav");
					sd.play(0);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedAudioFileException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (LineUnavailableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});thread.start();
	}
}