import java.lang.reflect.Array;
import java.util.ArrayList;


public class Grafo {
	private CustoUniforme c = new CustoUniforme();
	private ArrayList<Vertice> listaVertice = new ArrayList<Vertice>();
	private int valorAdaptacao;

	private CelulaMatriz[][] matrizMapa;
	
	public Grafo(CelulaMatriz[][] matrizMapa, int valorAdaptacao){
		this.matrizMapa = matrizMapa;
		this.valorAdaptacao = valorAdaptacao;
	}

	
	public void adicionaVertice(){
		for (int i = 0; i < matrizMapa.length; i++) {
			for (int j = 0; j < matrizMapa[0].length; j++) {
				
				listaVertice.add(new Vertice(i,j));

			}
		}
	}
	
	public int getValorAdaptacao() {
		return valorAdaptacao;
	}

	public void setValorAdaptacao(int valorAdaptacao) {
		this.valorAdaptacao = valorAdaptacao;
	}
	
	public CustoUniforme getC() {
		return c;
	}

	public void setC(CustoUniforme c) {
		this.c = c;
	}
	public ArrayList<Vertice> getListaVertice() {
		return listaVertice;
	}

	public void setListaVertice(ArrayList<Vertice> listaVertice) {
		this.listaVertice = listaVertice;
	}
	
	public Vertice getVertice(int coordenadaX, int coordenadaY){
		int indice = ((coordenadaX/valorAdaptacao) * matrizMapa[0].length) + (coordenadaY/valorAdaptacao);
		return listaVertice.get(indice);
	}
	
	public void adicionaAresta(){
		for(Vertice vertice : listaVertice){
			int coordenadaX = vertice.getCoordenadaX();
			int coordenadaY = vertice.getCoordenadaY();
			try{
			if(matrizMapa[coordenadaX][coordenadaY].getValorCelula() != 1){
				if(matrizMapa[coordenadaX-1][coordenadaY].getValorCelula() != 1){
					int indice = ((coordenadaX-1) * matrizMapa[0].length) + coordenadaY;
					vertice.addAresta(new Aresta(listaVertice.get(indice), matrizMapa[coordenadaX-1][coordenadaY].getCustoCelula()));
				}
				if(matrizMapa[coordenadaX+1][coordenadaY].getValorCelula() != 1){
					int indice = ((coordenadaX+1) * matrizMapa[0].length) + coordenadaY;
					vertice.addAresta(new Aresta(listaVertice.get(indice), matrizMapa[coordenadaX+1][coordenadaY].getCustoCelula()));;
				}
				if(matrizMapa[coordenadaX][coordenadaY-1].getValorCelula() != 1){
					int indice = ((coordenadaX) * matrizMapa[0].length) + (coordenadaY-1);
					vertice.addAresta(new Aresta(listaVertice.get(indice), matrizMapa[coordenadaX][coordenadaY-1].getCustoCelula()));
				}
				if(matrizMapa[coordenadaX][coordenadaY+1].getValorCelula() != 1){
					int indice = ((coordenadaX) * matrizMapa[0].length) + (coordenadaY+1);
					vertice.addAresta(new Aresta(listaVertice.get(indice), matrizMapa[coordenadaX][coordenadaY+1].getCustoCelula()));
				}
			}
			}catch(ArrayIndexOutOfBoundsException e){
				
			}
			
		}
	}
	
	public Vertice buscaVertice(String label){
		for (Vertice vertice : listaVertice) {
			if (vertice.getLabel().equals(label)) {
				return vertice;
			}
		}
		return null;
	}
	
	public void exibeArestas(){
		for (Vertice vertice : listaVertice){
			vertice.exibeArestas();
		}
	}

	public void reinicializa(){
		for(Vertice vertice : listaVertice){
			vertice.setPaisNull();
			vertice.setCustoMinimo(0);
			vertice.setExpandido(false);
		}
	}
	
}
