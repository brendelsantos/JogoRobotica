
public class CelulaMatriz {

	private int valorCelula;
	private int custoCelula;
	
	public CelulaMatriz(int valorCelula, int custoCelula) {
		this.valorCelula = valorCelula;
		this.custoCelula = custoCelula;
	}

	public int getValorCelula() {
		return valorCelula;
	}

	public void setValorCelula(int valorCelula) {
		this.valorCelula = valorCelula;
	}

	public int getCustoCelula() {
		return custoCelula;
	}

	public void setCustoCelula(int custoCelula) {
		this.custoCelula = custoCelula;
	}
	
}
