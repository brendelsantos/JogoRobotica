import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class Personagem {

	final int VALOR_ADAPTACAO = 5;
	private int altura;
	private int largura;
	private Image personagem;
	private int coordenadaX;
	private int coordenadaY;
	private char pernaMovimento;
	private URL path;
	private Image[] images;
	private int valorPersonagem;

	public Personagem(int altura, int largura, int coordenadaX, int coordenadaY) {
		this.altura = altura;
		this.largura = largura;
		this.coordenadaX = coordenadaX;
		this.coordenadaY = coordenadaY;
		pernaMovimento = 'D';
		images = new Image[8];
		// CarregaImagens(nomeArquivo);
	}

	public int getCoordenadaX() {
		return coordenadaX;
	}

	public void setCoordenadaX(int coordenadaX) {
		this.coordenadaX = coordenadaX;
	}

	public int getCoordenadaY() {
		return coordenadaY;
	}

	public void setCoordenadaY(int coordenadaY) {
		this.coordenadaY = coordenadaY;
	}

	public Image getPersonagem() {
		return personagem;
	}

	public void setValorPersonagem(int valorPersonagem) {
		this.valorPersonagem = valorPersonagem;
	}

	public int getValorPersonagem() {
		return valorPersonagem;
	}
	
	public void CarregaImagens(String nomeArquivo) {
		try {
			for (int i = 0; i < images.length; i++) {
				path = getClass().getResource(
						"/images/" + nomeArquivo + i + ".png");
				images[i] = ImageIO.read(path).getScaledInstance(largura,
						altura, personagem.SCALE_DEFAULT);
			}
			personagem = images[6];

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void alteraImagem(Direcao direcao) {
		int indiceImagemAtual = direcao.getValue();

		if (pernaMovimento == 'D') {
			indiceImagemAtual = direcao.getValue() + 1;
			pernaMovimento = 'E';
		} else {
			pernaMovimento = 'D';

		}
		personagem = images[indiceImagemAtual];
	}

	public void alteraPosicao(CelulaMatriz[][] matrizMapa, int coordenadaX,
			int coordenadaY) {

		testaPosicaoMatriz(matrizMapa, coordenadaX, coordenadaY);

	}

	public void testaPosicaoMatriz(CelulaMatriz[][] matrizMapa, int coordenadaX,
			int coordenadaY) {
		try {		
		//	System.out.println(coordenadaX / VALOR_ADAPTACAO+" "+coordenadaY
			//		/ VALOR_ADAPTACAO);
			boolean celulaLivre = matrizMapa[coordenadaX / VALOR_ADAPTACAO][coordenadaY
					/ VALOR_ADAPTACAO].getValorCelula() == 0;
			boolean celulaPersonagem = matrizMapa[coordenadaX / VALOR_ADAPTACAO][coordenadaY
					/ VALOR_ADAPTACAO].getValorCelula() == valorPersonagem;
			if (celulaLivre || celulaPersonagem) {
				this.coordenadaX = coordenadaX;
				this.coordenadaY = coordenadaY;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e.getStackTrace());
		}
	}
	public void alteraValorMatriz(CelulaMatriz[][] matrizMapa) {
		substituiValorAntigo(matrizMapa);		
		marcaValorPersonagemAtual(matrizMapa);
	}

	private void marcaValorPersonagemAtual(CelulaMatriz[][] matrizMapa) {
		int quadranteInicioX = (coordenadaX / VALOR_ADAPTACAO) - 2;
		int quadranteFimX = (coordenadaX / VALOR_ADAPTACAO) + 6;
		for (int i = quadranteInicioX; i < quadranteFimX; i++) {
			int quadranteInicioY = (coordenadaY / VALOR_ADAPTACAO) - 10;
			int quadranteFimY = (coordenadaY / VALOR_ADAPTACAO) + 7;
			for (int j = quadranteInicioY; j < quadranteFimY; j++) {
				try {
					boolean dentroDoMapa = (i > 0) && (i < matrizMapa.length) && (j > 0) && (j < matrizMapa[0].length);
					if(dentroDoMapa){
						if(valorPersonagem == 0)
							matrizMapa[i][j].setValorCelula(valorPersonagem);
						if(matrizMapa[i][j].getValorCelula() != 1 && matrizMapa[i][j].getValorCelula() == 0){	
							matrizMapa[i][j].setValorCelula(valorPersonagem);
						}
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void substituiValorAntigo(CelulaMatriz[][] matrizMapa) {
		int quadranteInicioX = (coordenadaX / VALOR_ADAPTACAO) - 15;
		int quadranteFimX = (coordenadaX / VALOR_ADAPTACAO) +19;
		for (int i = quadranteInicioX; i < quadranteFimX; i++) {
			int quadranteInicioY = (coordenadaY / VALOR_ADAPTACAO) - 24;
			int quadranteFimY = (coordenadaY / VALOR_ADAPTACAO) + 20;
			for (int j = quadranteInicioY; j < quadranteFimY; j++) {
				boolean dentroDoMapa = (i > 0) && (i < matrizMapa.length) && (j > 0) && (j < matrizMapa[0].length);
				if(dentroDoMapa){
					if((matrizMapa[i][j].getValorCelula() != 1) && (matrizMapa[i][j].getValorCelula() == valorPersonagem)) {
						matrizMapa[i][j].setValorCelula(0);
					}
				}
			}
		}
	}
	public void paint(Graphics g) {
		g.drawImage(personagem, coordenadaX, coordenadaY, null);
	}
}
