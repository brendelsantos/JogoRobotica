
public class MatrizMapa {
	private CelulaMatriz[][] matrizMapa;
	
	private int alturaMatriz;
	private int larguraMatriz;
	private int larguraPanel;
	private int alturaPanel;


	public MatrizMapa(int laguraMatriz, int alturaMatriz, int larguraPanel, int alturaPanel ) {
		this.larguraMatriz = laguraMatriz;
		this.alturaMatriz = alturaMatriz;
		this.larguraPanel = larguraPanel;
		this.alturaPanel = alturaPanel;
		System.out.println(larguraPanel +" "+alturaPanel);
		matrizMapa = new CelulaMatriz[this.larguraMatriz][this.alturaMatriz];
		inicializaMatriz();
		preencheMatriz();
	}
	
	public void inicializaMatriz(){
		for (int i = 0; i < this.larguraMatriz; i++) {
			for (int j = 0; j < this.alturaMatriz; j++) {
				matrizMapa[i][j] = new CelulaMatriz(0, 0);
			}
		}
	}	
	public void preencheMatriz() {
		for (int i = 0; i < larguraMatriz; i++) {
			matrizMapa[i][0].setValorCelula(1);
			matrizMapa[i][alturaMatriz - 1].setValorCelula(1);
		}
		for (int i = 0; i < alturaMatriz; i++) {
			matrizMapa[0][i].setValorCelula(1);
			matrizMapa[larguraMatriz - 1][i].setValorCelula(1);
		}

		
		for (int i = porcentagemLargura(8.5); i < porcentagemLargura(12.75); i++) {
			for (int j = 0; j < porcentagemAltura(3.75); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i = porcentagemLargura(3.75); i < porcentagemLargura(7.75); i++) {
			for (int j = porcentagemAltura(3.125); j < porcentagemAltura(7.124999999999999); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i = porcentagemLargura(13.083333333333332); i < porcentagemLargura(17.083333333333332); i++) {
			for (int j = porcentagemAltura(9.25); j < porcentagemAltura(14.249999999999998); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i = porcentagemLargura(13.083333333333332); i < porcentagemLargura(14.166666666666666); i++) {
			for (int j = porcentagemAltura(9.25); j < porcentagemAltura(10.625); j++) {
				matrizMapa[i][j].setValorCelula(0);
			}
		}
		
		for (int i = porcentagemLargura(16); i < porcentagemLargura(17.083333333333332); i++) {
			for (int j = porcentagemAltura(9.25); j < porcentagemAltura(10.625); j++) {
				matrizMapa[i][j].setValorCelula(0);
			}
		}
		
		for (int i = porcentagemLargura(16); i < porcentagemLargura(17.083333333333332); i++) {
			for (int j = porcentagemAltura(13.25); j < porcentagemAltura(14.249999999999998); j++) {
				matrizMapa[i][j].setValorCelula(0);
			}
		}
		for (int i = porcentagemLargura(13.083333333333332); i < porcentagemLargura(14.000000000000002); i++) {
			for (int j = porcentagemAltura(13.125); j < porcentagemAltura(14.249999999999998); j++) {
				matrizMapa[i][j].setValorCelula(0);
			}
		}
		for (int i = porcentagemLargura(10.333333333333334); i < porcentagemLargura(12.5); i++) {
			for (int j = porcentagemAltura(10.125); j < porcentagemAltura(12.875); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		int inicioLargura = porcentagemLargura(11.416666666666666);
		int inicioAltura = porcentagemAltura(10.125);
		while (inicioLargura < porcentagemLargura(12.5) && inicioAltura < porcentagemAltura(12)) {
			for (int i = inicioLargura; i < porcentagemLargura(12.5); i++) {
				matrizMapa[i][inicioAltura].setValorCelula(0);
			}
			inicioAltura++;
			inicioLargura++;
		}		
			
		inicioLargura = porcentagemLargura(11.416666666666666);
		inicioAltura = porcentagemAltura(10.125);
		while (inicioLargura >= porcentagemLargura(10.333333333333334) && inicioAltura < porcentagemAltura(12)) {
			for (int i = inicioLargura; i >= porcentagemLargura(10.333333333333334); i--) {
				matrizMapa[i][inicioAltura].setValorCelula(0);
			}
			inicioAltura++;
			inicioLargura--;
		}	
		
		for (int i = porcentagemLargura(6.666666666666667); i < porcentagemLargura(7.416666666666667); i++) {
			for (int j = porcentagemAltura(12.375); j <porcentagemAltura(13.375); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i = 0; i < porcentagemLargura(0.5833333333333334); i++) {
			for (int j = porcentagemAltura(11.75); j < alturaMatriz; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i = 0; i < porcentagemLargura(2.666666666666667); i++) {
			for (int j = porcentagemAltura(5); j < porcentagemAltura(10.625); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		inicioLargura = porcentagemLargura(1.7500000000000002);
		inicioAltura = porcentagemAltura(4.875);
		while (inicioLargura < porcentagemLargura(2.666666666666667) && inicioAltura < porcentagemAltura(6.875000000000001)) {
			for (int i = inicioLargura; i < porcentagemLargura(2.666666666666667); i++) {
				matrizMapa[i][inicioAltura].setValorCelula(0);
			}
			inicioAltura++;
			inicioLargura++;
		}	
		for (int i = 0; i < porcentagemLargura(1.8333333333333333); i++) {
			for (int j = porcentagemAltura(10.625); j < porcentagemAltura(11.75); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i =  porcentagemLargura(14.249999999999998); i <  porcentagemLargura(16.25); i++) {
			for (int j = porcentagemAltura(0.8750000000000001); j < porcentagemAltura(3.25); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		for (int i =  porcentagemLargura(16.5); i <  porcentagemLargura(19.083333333333332); i++) {
			for (int j = porcentagemAltura(13.25); j < alturaMatriz; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		
		inicioLargura = porcentagemLargura(18.9);
		inicioAltura = porcentagemAltura(13.324);
		while (inicioLargura >= porcentagemLargura(16.5) && inicioAltura < alturaMatriz) {
			for (int i = inicioLargura; i >= porcentagemLargura(16.5)
					; i--) {
				matrizMapa[i][inicioAltura].setValorCelula(0);
			}
			inicioAltura++;
			inicioLargura--;
		}
		for (int i =  porcentagemLargura(17.083333333333332); i <  porcentagemLargura(19.083333333333332); i++) {
			for (int j = porcentagemAltura(11); j < porcentagemAltura(12.375); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = 0; i <  porcentagemLargura(1.5833333333333335); i++) {
			for (int j = 0; j < porcentagemAltura(4.875); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i =  porcentagemLargura(17.666666666666668); i <  porcentagemLargura(19.083333333333332); i++) {
			for (int j = porcentagemAltura(0.125); j < porcentagemAltura(1); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i =  porcentagemLargura(2.4166666666666665); i <  porcentagemLargura(3.5000000000000004); i++) {
			for (int j = porcentagemAltura(12.625); j < porcentagemAltura(13.5); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = porcentagemLargura(2.4166666666666665) ; i <  porcentagemLargura(4.416666666666667); i++) {
			for (int j = porcentagemAltura(14.000000000000002); j < porcentagemAltura(14.875); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i =  porcentagemLargura(2.4166666666666665); i <  porcentagemLargura(5.416666666666667); i++) {
			for (int j = porcentagemAltura(15.375); j < porcentagemAltura(16.25); j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = porcentagemLargura(6.583333333333333); i < porcentagemLargura(7.583333333333334); i++) {
			for (int j = porcentagemAltura(16.5); j < alturaMatriz; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = porcentagemLargura(10.583333333333334); i < porcentagemLargura(11.75); i++) {
			for (int j = porcentagemAltura(16.25); j < alturaMatriz; j++) {
				matrizMapa[i][j].setValorCelula(1);
			}
		}
		for (int i = porcentagemLargura(17.666666666666668); i < porcentagemLargura(18.75); i++) {
			for (int j = porcentagemAltura(1.25); j < porcentagemAltura(9.125); j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = porcentagemLargura(16.5) ; i < porcentagemLargura(17.75); i++) {
			for (int j = porcentagemAltura(5); j < porcentagemAltura(7.5); j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = porcentagemLargura(15.666666666666668); i < porcentagemLargura(16.583333333333332); i++) {
			for (int j = porcentagemAltura(5); j < porcentagemAltura(6.375); j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = porcentagemLargura(10.75); i < porcentagemLargura(13); i++) {
			for (int j = porcentagemAltura(6.875000000000001); j < porcentagemAltura(9.75); j++) {
				matrizMapa[i][j].setCustoCelula(5);
			}
		}
		for (int i = porcentagemLargura(8.416666666666666); i < porcentagemLargura(9.416666666666666); i++) {
			for (int j = porcentagemAltura(8.25); j < porcentagemAltura(11); j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = porcentagemLargura(6.916666666666667); i < porcentagemLargura(7.75); i++) {
			for (int j = porcentagemAltura(8.375); j < porcentagemAltura(9.875); j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = porcentagemLargura(5.666666666666666); i < porcentagemLargura(6.666666666666667); i++) {
			for (int j = porcentagemAltura(8.625); j < porcentagemAltura(9.875); j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = porcentagemLargura(3.5000000000000004); i < porcentagemLargura(4.416666666666667); i++) {
			for (int j = porcentagemAltura(9.875); j < porcentagemAltura(11.25); j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = porcentagemLargura(2.5); i < porcentagemLargura(5.416666666666667); i++) {
			for (int j = porcentagemAltura(10.875); j < porcentagemAltura(12.5); j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = porcentagemLargura(3.5000000000000004); i < porcentagemLargura(6.416666666666666); i++) {
			for (int j = porcentagemAltura(12.5); j < porcentagemAltura(13.750000000000002); j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = porcentagemLargura(4.416666666666667); i < porcentagemLargura(7.166666666666667) ; i++) {
			for (int j =porcentagemAltura(13.875000000000002) ; j < porcentagemAltura(14.75); j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = porcentagemLargura(9.75); i < porcentagemLargura(16.666666666666664); i++) {
			for (int j = porcentagemAltura(16); j < alturaMatriz; j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
		for (int i = porcentagemLargura(17.083333333333332); i < porcentagemAltura(18.75); i++) {
			for (int j =porcentagemLargura(8.333333333333332); j <porcentagemAltura(15); j++) {
				matrizMapa[i][j].setCustoCelula(3);
			}
		}
	}
	public int getLarguraPanel() {
		return larguraPanel;
	}

	public void setLarguraPanel(int larguraPanel) {
		this.larguraPanel = larguraPanel;
	}

	public int getAlturaPanel() {
		return alturaPanel;
	}

	public void setAlturaPanel(int alturaPanel) {
		this.alturaPanel = alturaPanel;
	}
	
	public CelulaMatriz[][] getMatrizMapa() {
		return matrizMapa;
	}
	
	public int porcentagemLargura(double valorPorcentagem){
		return (int)((valorPorcentagem / 100) * larguraPanel) ;
	}
	
	public int porcentagemAltura(double valorPorcentagem){
		return (int) ((valorPorcentagem / 100) * alturaPanel);
	}
	
	
	public CelulaMatriz[][] atualizaMatriz(){
		for (int i = 0; i < larguraMatriz; i++) {
			for (int j = 0; j < alturaMatriz; j++) {
				if(matrizMapa[i][j].getValorCelula() != 1 && matrizMapa[i][j].getValorCelula() != 0)
					matrizMapa[i][j].setValorCelula(0);;
			}
		}
		return matrizMapa;
	}

	public void imprimeMatriz(){
		for (int i = 0; i < larguraMatriz; i++) {
			for (int j = 0; j < alturaMatriz; j++) {
				System.out.print(matrizMapa[i][j].getValorCelula());
			}
			System.out.println();
		}
		System.out.println();
	}
}
