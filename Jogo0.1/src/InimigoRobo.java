import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JProgressBar;


public class InimigoRobo extends Personagem implements Runnable{
	
	private int vida;
	private JProgressBar vidaInimigo;	
	private Player player;
	private Grafo grafo;
	boolean atacou;
	private int valorAdaptacao;
	private boolean threadAtiva;
	Semaphore mutex = new Semaphore(1);
	
	public InimigoRobo(int altura, int largura, int coordenadaX, int coordenadaY, Player player, Grafo grafo) {
		super(altura, largura, coordenadaX, coordenadaY);
		vidaInimigo = new JProgressBar();
		vida = 100;
		vidaInimigo.setValue(vida);
		atacou = true;
		threadAtiva = true;
		this.player = player;
		this.grafo = grafo;
		super.CarregaImagens("monster");
	}
	
	public int getVida() {
		return vida;
	}

	public void setVida(int vida) {
		this.vida = vida;
	}

	public int getValorAdaptacao() {
		return valorAdaptacao;
	}

	public void setValorAdaptacao(int valorAdaptacao) {
		this.valorAdaptacao = valorAdaptacao;
	}
	
	public boolean isThreadAtiva() {
		return threadAtiva;
	}

	public void setThreadAtiva(boolean threadAtiva) {
		this.threadAtiva = threadAtiva;
	}

	public void paint(Graphics g, PanelPrincipal moveImagem) {
		if(!morreu()){
			g.drawImage(getPersonagem(), getCoordenadaX(), getCoordenadaY(), null);
			vidaInimigo.setValue(vida);
			moveImagem.add(vidaInimigo);
			vidaInimigo.setBounds(getCoordenadaX(), getCoordenadaY()-VALOR_ADAPTACAO, 60, 5);
		}
		if(morreu() || getValorPersonagem() == 0){
			moveImagem.remove(vidaInimigo);
		}
	}

	public void consomeVida(){
		vida -= 7;
	}
	
	public boolean morreu(){
		if(vida <= 0){
			setValorPersonagem(0);
			return true;
		}else{
			
			return false;
		}
	}
	
	public void anguloRelacaoPlayer(int playerX, int playerY){
		double x = playerX - getCoordenadaX();
		double y = playerY - getCoordenadaY();
		double anguloRadianos = Math.atan2(x, y	);	
		double anguloGraus = Math.toDegrees(anguloRadianos);
		if(anguloGraus < 0){
			anguloGraus = Math.toDegrees(anguloRadianos) + 360;
		}
		alteraImagem(testaDirecao(anguloGraus));
	}
	
	public Direcao testaDirecao(double angulo){
		if((angulo >= 315 && angulo <= 360) || (angulo >= 0 && angulo < 45))
			return Direcao.BAIXO;
		else if((angulo >= 45) && (angulo < 135))
			return Direcao.DIREITA;
		else if((angulo >= 135) && (angulo < 225))
			return Direcao.CIMA;
		else if((angulo >= 225) && (angulo < 315))
			return Direcao.ESQUERDA;
		return null;
	}
	
	double distanciaAtePlayer(){

	    return Math.sqrt((Math.pow((player.getCoordenadaX() - this.getCoordenadaX()),2))+
	                (Math.pow((player.getCoordenadaY() - this.getCoordenadaY()),2)));
	}
	
	private void somGolpe() {
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				SoundTest sd = new SoundTest();
				try {
					sd.load("ataque_.wav");
					sd.play(0);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedAudioFileException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (LineUnavailableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});thread.start();
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while(threadAtiva){
			if(morreu()){
				return;
			}
			atacou = false;
			grafo.getC().custoUniforme(grafo.getVertice(getCoordenadaX(), getCoordenadaY()),grafo.getVertice(player.getCoordenadaX(), player.getCoordenadaY()), getValorPersonagem());
			ArrayList<Vertice> caminho = null;
			try{		
				caminho = grafo.getC().geraCaminho(grafo.getVertice(player.getCoordenadaX(), player.getCoordenadaY()), getValorPersonagem());
				grafo.reinicializa();
				try {
					(mutex).acquire();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int i = 0; i < caminho.size(); i++){
					if(this != null){
						this.setCoordenadaX(caminho.get(i).getCoordenadaX()*valorAdaptacao);
						this.setCoordenadaY(caminho.get(i).getCoordenadaY()*valorAdaptacao);
						if(this.morreu()){
							i = caminho.size();
						}
						if(distanciaAtePlayer() < 45 && !atacou){
							somGolpe();
							atacou = true;
							
						}
						try {
							Thread.sleep(24);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}					
					}
				}
				(mutex).release();
				
			}catch(NullPointerException e){
				return;
			}
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
